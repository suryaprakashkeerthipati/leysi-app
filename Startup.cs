using Hangfire;
using Hangfire.PostgreSql;
using Leysi.Schedulers.Data;
using Leysi.Schedulers.Data.Context;
using Leysi.Schedulers.Data.Repository;
using Leysi.Schedulers.EventListners;
using Leysi.Schedulers.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Leysi.Schedulers
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IConfiguration _configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHangfire(h => h.UsePostgreSqlStorage(_configuration.GetConnectionString("LeysiConnectionString")));
            services.AddHangfireServer();
            services.AddHttpClient();
            services.AddControllers();
            services.AddLogging();
            services.AddScoped<IRestService, RestService>();
            services.AddScoped<IUsersServices, UsersServices>();
            services.AddScoped<IReviewsServices, ReviewsServices>();
            services.AddScoped<IVotesServices, VotesServices>();
            services.AddScoped<ICommentsServices, CommentsServices>();
            services.AddScoped<IFollowersServices, FollowersServices>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IReviewsRepository, ReviewsRepository>();
            services.AddScoped<IVotesRepository, VotesRepository>();
            services.AddScoped<ICommentsRepository, CommentsRepository>();
            services.AddScoped<IFollowersRepository, FollowersRepository>();
            services.AddScoped<IFakeRepository, FakeRepository>();
            services.AddScoped<ILeysiDbContext, LeysiDbContext>();
            services.AddScoped<FakeDBContext>();
            services.AddDbContext<FakeDBContext>(options => options.UseNpgsql(_configuration.GetConnectionString("LeysiConnectionString")));
            services.AddDbContext<LeysiDbContext>(options => options.UseNpgsql(_configuration.GetConnectionString("LeysiConnectionString")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseHangfireDashboard("/scheduler-dashboard");

             app.UsePostgressEventListnerForUsers();
            app.UsePostgreEventListnerForVotes();
            app.UsePostgressEventListnerForFollowers();
            app.UsePostgreEventListnerForReviews();
            app.UsePostgreEventListnerForComments();


            RecurringJob.AddOrUpdate<IUsersServices>(x => x.ExecuteIndexing(), "10 1 * * ?");
            RecurringJob.AddOrUpdate<IReviewsServices>(x => x.ExecuteIndexing(), "10 2 * * ?");
            RecurringJob.AddOrUpdate<IVotesServices>(x => x.ExecuteIndexing(), "10 3 * * ?");
            RecurringJob.AddOrUpdate<IFollowersServices>(x => x.ExecuteIndexing(), "10 4 * * ?");
            RecurringJob.AddOrUpdate<ICommentsServices>(x => x.ExecuteIndexing(), "10 5 * * ?");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(name: "default", pattern: "v1/{controller}/{action}/{id?}");
            });
        }
    }
}
