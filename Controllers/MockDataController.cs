﻿using Leysi.Schedulers.Data.Repository;
using Leysi.Schedulers.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class MockDataController : ControllerBase
    {
        private readonly IFakeRepository _fakeRepository;
        private readonly ILogger<MockDataController> _logger;
        private readonly IUsersServices _indexingServices;

        public MockDataController(IFakeRepository fakeRepository, ILogger<MockDataController> logger, IUsersServices indexingServices)
        {
            _fakeRepository = fakeRepository;
            _logger = logger;
            _indexingServices = indexingServices;
        }

        [HttpGet("insertusersmock")]
        public async Task<IActionResult> InsertUsersMockData(int batchSize)
        {
            var response = await _fakeRepository.InsertUsersMockData(batchSize);
            return Ok(response);
        }

        [HttpGet("insertreviewsmock")]
        public async Task<IActionResult> InsertReviewsMock(int batchSize)
        {
            var response = await _fakeRepository.InsertReviewsData(batchSize);
            return Ok(response);
        }

        [HttpGet("insertvotesmock")]
        public async Task<IActionResult> InsertVotes(int batchSize)
        {
            var response = await _fakeRepository.InsertVotesMockData(batchSize);
            return Ok(response);
        }
        [HttpGet("insertfollowersmock")]
        public async Task<IActionResult> InsertFollowers(int batchSize)
        {
            var response = await _fakeRepository.InserFollowersMockData(batchSize);
            return Ok(response);
        }

        [HttpGet("insertcommentsmock")]
        public async Task<IActionResult> InsertComments(int batchSize)
        {
            var response = await _fakeRepository.InsertCommentsMockData(batchSize);
            return Ok(response);
        }
        [HttpGet("healthcheck")]
        public IActionResult HealthCheck()
        {
            return Ok();
        }
    }
}
