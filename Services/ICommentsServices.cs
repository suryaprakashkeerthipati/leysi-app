﻿using System.Threading.Tasks;

namespace Leysi.Schedulers.Services
{
    public interface ICommentsServices
    {
        Task ExecuteIndexing();
    }
}
