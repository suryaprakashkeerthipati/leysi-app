﻿using System.Threading.Tasks;

namespace Leysi.Schedulers.Services
{
    public interface IFollowersServices
    {
        Task ExecuteIndexing();
    }
}
