﻿using Leysi.Schedulers.Data.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Services
{
    public class FollowersServices : IFollowersServices
    {
        private readonly IFollowersRepository _repository;
        private readonly ILogger<FollowersServices> _log;
        private readonly IRestService _restService;
        private readonly IConfiguration _configuration;
        public FollowersServices(IFollowersRepository repository, ILogger<FollowersServices> log, IRestService restService, IConfiguration configuration)
        {
            _repository = repository;
            _log = log;
            _restService = restService;
            _configuration = configuration;
        }
        public async Task ExecuteIndexing()
        {
            var indexedRecords = await _repository.GetIndexedRecords();
            if (indexedRecords == null || indexedRecords.Count == 0)
            {
                _log.LogInformation($"Followers - No Records exists to process indexing");
                return;
            }
            var baseUrl = $"{_configuration["LeysiSearchServiceBaseUrl"]}followers";
            int totalRecords = indexedRecords.Count;
            int processedRecords = 0;

            foreach (var document in indexedRecords)
            {
                var results = await _restService.SendAsync(baseUrl, document);
                if (results.IsSuccess == true && (results.HttpStatusCode == HttpStatusCode.OK || results.HttpStatusCode == HttpStatusCode.Created))
                {
                    document.isindexed = false;
                    var update = await _repository.UpdateIndexing(document);
                    if (update == 1)
                    {
                        processedRecords++;
                        _log.LogInformation($"Followers - Indexed got successed for user id :{document.id}");
                    }
                }
                else
                {
                    _log.LogError($"Followers - Indexed got failed for user id :{document.id}");
                }
            }
            _log.LogError($"Followers - Total indexed user documents  {processedRecords}/{totalRecords}");
        }
    }
}
