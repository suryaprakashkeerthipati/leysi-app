﻿using System.Threading.Tasks;

namespace Leysi.Schedulers.Services
{
    public interface IUsersServices
    {
        Task ExecuteIndexing();
    }
}
