﻿using LeysiSearch.API.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Services
{
    public class RestService : IRestService
    {
        private readonly IHttpClientFactory _httpClient;
        public RestService(IHttpClientFactory httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<HttpProcessResults<T>> SendAsync<T>(string apiUrl, T entity)
        {
            HttpProcessResults<T> httpProcessResults = null;
            Uri uri = new Uri(apiUrl);
            string jsonData = JsonConvert.SerializeObject(entity);
            StringContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

            HttpRequestMessage httpRequest = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = uri,
                Content = content
            };

            using (var client = _httpClient.CreateClient())
            {
                var response = await client.SendAsync(httpRequest);
                var responseContent = await response.Content.ReadAsStringAsync();
                httpProcessResults = JsonConvert.DeserializeObject<HttpProcessResults<T>>(responseContent);
            }

            return httpProcessResults;
        }
    }
}
