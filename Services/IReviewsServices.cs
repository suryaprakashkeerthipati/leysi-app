﻿using System.Threading.Tasks;

namespace Leysi.Schedulers.Services
{
    public interface IReviewsServices
    {
        Task ExecuteIndexing();
    }
}
