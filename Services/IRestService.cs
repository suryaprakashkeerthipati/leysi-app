﻿using LeysiSearch.API.Models;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Services
{
   public interface IRestService
    {
        Task<HttpProcessResults<T>> SendAsync<T>(string apiUrl, T entity);
    }
}
