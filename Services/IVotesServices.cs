﻿using System.Threading.Tasks;

namespace Leysi.Schedulers.Services
{
    public interface IVotesServices
    {
        Task ExecuteIndexing();
    }
}
