﻿using System.Net;

namespace LeysiSearch.API.Models
{
    public class HttpProcessResults<T>
    {
        private HttpStatusCode _httpStatusCode;
        private string _message;
        private T _result;
       
        public bool IsSuccess { get; set; }

        public HttpStatusCode HttpStatusCode
        {
            get { return _httpStatusCode; }
            set { _httpStatusCode = value; }
        }
        public T Result
        {
            get { return _result; }
            set
            {
                _result = value;
                IsSuccess = true;
            }
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

    }

}


