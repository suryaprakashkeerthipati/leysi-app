﻿using System.Net;

namespace Leysi.Schedulers.Models
{
    public class SchedulerResponse
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
