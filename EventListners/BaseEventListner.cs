﻿using LeysiSearch.API.Models;
using Nest;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Leysi.Schedulers.EventListners
{
    public class BaseEventListner
    {
        protected async Task<HttpProcessResults<IndexResponse>> SendAsync<T>(string apiUrl, T entity)
        {
            HttpProcessResults<IndexResponse> httpProcessResults = null;
            Uri uri = new Uri(apiUrl);
            string jsonData = JsonConvert.SerializeObject(entity);
            StringContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

            HttpRequestMessage httpRequest = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = uri,
                Content = content
            };

            using (var client = new HttpClient())
            {
                var response = await client.SendAsync(httpRequest);
                var responseContent = await response.Content.ReadAsStringAsync();
                httpProcessResults = JsonConvert.DeserializeObject<HttpProcessResults<IndexResponse>>(responseContent);
            }

            return httpProcessResults;
        }
        protected async Task<HttpProcessResults<DeleteResponse>> DeleteAsync(string apiUrl)
        {
            HttpProcessResults<DeleteResponse> httpProcessResults = null;
            Uri uri = new Uri(apiUrl);
           
            using (var client = new HttpClient())
            {
                var response = await client.DeleteAsync(uri);
                var responseContent = await response.Content.ReadAsStringAsync();
                httpProcessResults = JsonConvert.DeserializeObject<HttpProcessResults<DeleteResponse>>(responseContent);
            }

            return httpProcessResults;
        }
    }
}
