﻿using Leysi.Schedulers.Data.Entities;

namespace Leysi.Schedulers.EventListners.Models
{
    public class UsersInfo
    {
        public string table { get; set; }
        public string action { get; set; }
        public Users data { get; set; }
    }
}
