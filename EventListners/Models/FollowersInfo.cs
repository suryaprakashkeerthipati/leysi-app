﻿using Leysi.Schedulers.Data.Entities;

namespace Leysi.Schedulers.EventListners.Models
{
    public class FollowersInfo
    {
        public string table { get; set; }
        public string action { get; set; }
        public Followers data { get; set; }
    }
}
