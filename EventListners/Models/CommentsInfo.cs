﻿using Leysi.Schedulers.Data.Entities;

namespace Leysi.Schedulers.EventListners.Models
{
    public class CommentsInfo
    {
        public string table { get; set; }
        public string action { get; set; }
        public Comments data { get; set; }
    }
}
