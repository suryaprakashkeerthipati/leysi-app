﻿using Leysi.Schedulers.Data.Entities;

namespace Leysi.Schedulers.EventListners.Models
{
    public class VotesInfo
    {
        public string table { get; set; }
        public string action { get; set; }
        public Votes data { get; set; }
    }
}
