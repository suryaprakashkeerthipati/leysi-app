﻿using Leysi.Schedulers.Data.Entities;

namespace Leysi.Schedulers.EventListners.Models
{
    public class ReviewsInfo
    {
        public string table { get; set; }
        public string action { get; set; }
        public Reviews data { get; set; }
    }
}
