﻿using Leysi.Schedulers.EventListners.Models;
using Microsoft.AspNetCore.Builder;
using Newtonsoft.Json;
using Npgsql;
using System.Data;
using System.Threading.Tasks;

namespace Leysi.Schedulers.EventListners
{
    public static class FollowersEventListner
    {
        public static async Task UsePostgressEventListnerForFollowers(this IApplicationBuilder builder)
        {
            var listner = new PostgressFollowersListner();
            await listner.FollowersEventNotification();
        }
    }

    internal class PostgressFollowersListner : BaseEventListner
    {
        const string apiUrl = "https://d4qz9jla9d.execute-api.us-east-2.amazonaws.com/v1/followers";
        const string leysiConnectionString = "Server=leysi-staging.c2gcydmfdjpw.us-east-1.rds.amazonaws.com;Port=5432;Database=postgres;UserId=postgres;Password=LeysiPassword1;Timeout=60;";
        public async Task FollowersEventNotification()
        {
            await using (var connectionString = new NpgsqlConnection(leysiConnectionString))
            {
                await connectionString.OpenAsync();
                connectionString.Notification += FollowersEventListner;
                await using (var command = new NpgsqlCommand())
                {
                    command.CommandText = "LISTEN followersdatachange;";
                    command.CommandType = CommandType.Text;
                    command.Connection = connectionString;
                    command.ExecuteNonQuery();
                }

                while (true)
                {
                    connectionString.Wait();
                }
            }
        }

        private void FollowersEventListner(object sender, NpgsqlNotificationEventArgs e)
        {
            var dataPayload = JsonConvert.DeserializeObject<FollowersInfo>(e.Payload);

            if (dataPayload.table.ToLower() == "followers" && (dataPayload.action.ToLower() == "insert" || dataPayload.action.ToLower() == "update"))
            {
                if (dataPayload.data != null)
                {
                    var result = SendAsync(apiUrl, dataPayload.data).Result;
                }
            }
            else if (dataPayload.table.ToLower() == "followers" && dataPayload.action.ToLower() == "delete")
            {
                var url = $"{apiUrl}?id={dataPayload.data.id}";
                var result = DeleteAsync(url).Result;
            }

        }


    }
}
