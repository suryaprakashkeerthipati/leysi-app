﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Leysi.Schedulers.Data.Entities
{
    [Table("comments")]
    public class Comments
    {
        public Comments(int id)
        {
            this.id = id;
        }
        [Key]          
        public int id { get; set; }             
        public int review_id { get; set; }     
        public int user_id { get; set; }   
        public string content { get; set; }        
        public DateTime created_at { get; set; }     
        public bool isindexed { get; set; }
    }
}
