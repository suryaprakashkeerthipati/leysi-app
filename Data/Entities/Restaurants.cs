﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Leysi.Schedulers.Data.Entities
{
    [Table("restaurants")]
    public class Restaurants
    {
        public Restaurants(int id)
        {
            Id = id;
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        
        public string Name { get; set; }

        [Column("contact_email")]
     
        public string Email { get; set; }
        [Column("street")]
        public string Street { get; set; }
        [Column("state")]
        public string State { get; set; }
        [Column("zipcode")]
        public int Zipcode { get; set; }
        [Column("cognito_id")]
        public string CognitoId { get; set; }
        [Column("yelp_id")]
        public string YelpId { get; set; }
        [Column("profile_image_url")]
        public string ProfileImageUrl { get; set; }
        [Column("city")]
        public string City { get; set; }
        [Column("stripe_secret")]
        public string StripeSecret { get; set; }
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }
        [Column("isindexed")]
        public bool IsIndexed { get; set; }
    }
}
