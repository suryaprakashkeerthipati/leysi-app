﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Leysi.Schedulers.Data.Entities
{
    [Table("reviews")]
    public class Reviews
    {
        public Reviews(int id)
        {
            this.id = id;
        }              
        public int id { get; set; }        
        public string yelp_id { get; set; }       
        public int user_id { get; set; }          
        public string description { get; set; }      
        public string img_key { get; set; }
        public DateTime posted { get; set; }   
        public bool isindexed { get; set; }
    }
}
