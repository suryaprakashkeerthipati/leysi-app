﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Leysi.Schedulers.Data.Entities
{
    [Table("users")]
    public class Users
    {
        public Users(int id)
        {
            this.id = id;
        }

        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public bool active { get; set; }
        public string img_key { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public bool isindexed { get; set; }
    }
}
