﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Leysi.Schedulers.Data.Entities
{
    [Table("followers")]
    public class Followers
    {     
        public int id { get; set; }       
        public int user_id { get; set; }             
        public int following_user_id { get; set; } 
        public bool isindexed { get; set; }
    }
}
