﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace Leysi.Schedulers.Data.Entities
{
    [Table("votes")]
    public class Votes
    {        
        public int id { get; set; }     
        public int review_id { get; set; }     
        public int user_id { get; set; }         
        public bool likes { get; set; }      
        public bool isindexed { get; set; }
    }
}
