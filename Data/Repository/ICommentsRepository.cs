﻿using Leysi.Schedulers.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public interface ICommentsRepository
    {
        Task<List<Comments>> GetIndexedRecords();
        Task<int> UpdateIndexing(Comments entity);
    }
}
