﻿using Leysi.Schedulers.Models;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public interface IFakeRepository
    {
        Task<SchedulerResponse> InsertUsersMockData(int batchSize);
        Task<SchedulerResponse> InsertReviewsData(int batchSize);
        Task<SchedulerResponse> InsertVotesMockData(int batchSize);
        Task<SchedulerResponse> InserFollowersMockData(int batchSize);
        Task<SchedulerResponse> InsertCommentsMockData(int batchSize);
    }
}
