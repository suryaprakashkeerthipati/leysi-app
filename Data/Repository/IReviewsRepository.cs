﻿using Leysi.Schedulers.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public interface IReviewsRepository
    {
        Task<List<Reviews>> GetIndexedRecords();
        Task<int> UpdateIndexing(Reviews entity);
    }
}
