﻿using Leysi.Schedulers.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public interface IUsersRepository
    {
        Task<List<Users>> GetIndexedRecords();
        Task<int> UpdateIndexing(Users entity);
    }
}
