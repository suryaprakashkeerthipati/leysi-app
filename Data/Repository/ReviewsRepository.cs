﻿using Leysi.Schedulers.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public class ReviewsRepository : IReviewsRepository
    {
        private readonly ILeysiDbContext _dbContext;
        public ReviewsRepository(ILeysiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> UpdateIndexing(Reviews entity)
        {
            _dbContext.Review.Update(entity);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Reviews>> GetIndexedRecords()
        {
            var result = await _dbContext.Review.Where(u => u.isindexed == true).ToListAsync();
            return result;
        }      
    }
}
