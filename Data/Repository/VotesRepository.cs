﻿using Leysi.Schedulers.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public class VotesRepository : IVotesRepository
    {
        private readonly ILeysiDbContext _dbContext;
        public VotesRepository(ILeysiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> UpdateIndexing(Votes entity)
        {
            _dbContext.Vote.Update(entity);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Votes>> GetIndexedRecords()
        {
            var result = await _dbContext.Vote.Where(u => u.isindexed == true).ToListAsync();
            return result;
        }      
    }
}
