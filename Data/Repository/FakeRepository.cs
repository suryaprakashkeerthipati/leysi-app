﻿using Bogus;
using Leysi.Schedulers.Data.Context;
using Leysi.Schedulers.Data.Entities;
using Leysi.Schedulers.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Npgsql.Bulk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public class FakeRepository : IFakeRepository
    {
        private readonly FakeDBContext _dbContext;
        private readonly ILogger<FakeRepository> _logger;
        private readonly IConfiguration _configuration;
        public FakeRepository(FakeDBContext dbContext, ILogger<FakeRepository> logger, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _logger = logger;
            _configuration = configuration;
        }
        public async Task<SchedulerResponse> InsertUsersMockData(int batchSize)
        {
            SchedulerResponse schedulerResponse;

            var sequenceNumber = _dbContext.User.Max(u => u.id);
            int id = sequenceNumber;

            var faker = new Faker<Users>()
                            .CustomInstantiator(f => new Users(id++))
                            .RuleFor(o => o.name, f => f.Name.FirstName())
                            .RuleFor(o => o.email, (f, o) => f.Internet.Email(o.name))
                            .RuleFor(o => o.password, (f, o) => f.Internet.Password())
                            .RuleFor(o => o.img_key, (f, o) => f.Internet.Random.Guid().ToString())
                            .RuleFor(o => o.active, true)
                            .RuleFor(o => o.created_at, f => f.Date.Recent(100))
                            .RuleFor(o => o.updated_at, f => f.Date.Recent(100))
                            .RuleFor(o => o.isindexed, true);

            var entity = faker.Generate(batchSize);

            var isProcessed = await BulkInsertAsync(entity);

            schedulerResponse = new SchedulerResponse
            {
                HttpStatusCode = isProcessed ? HttpStatusCode.OK : HttpStatusCode.InternalServerError,
                IsSuccess = isProcessed,
                Message = isProcessed ? $"Users mock data got inserted success from {sequenceNumber} to {sequenceNumber + batchSize} sequence number" : $"Users mock data got failed"
            };

            return schedulerResponse;
        }
        public async Task<SchedulerResponse> InsertReviewsData(int batchSize)
        {
            SchedulerResponse schedulerResponse;

            var sequenceNumber = _dbContext.Review.Max(u => u.id);
            var yelpIds = _dbContext.Restaurant.Select(r => r.YelpId).ToList();
            var userIds = _dbContext.User.Select(r => r.id).ToList();
            int id = sequenceNumber;
            var cuisineList = _configuration.GetSection("Cuisine:Name").GetChildren();
            var itemsList = _configuration.GetSection("Cuisine:Prefix").GetChildren();
            var cusions = new List<string>();
            var itemsPrefix = new List<string>();

            foreach (var cuision in cuisineList)
            {
                if (!string.IsNullOrEmpty(cuision.Value))
                    cusions.Add(cuision.Value);
            }

            foreach (var prefix in itemsList)
            {
                if (!string.IsNullOrEmpty(prefix.Value))
                    itemsPrefix.Add(prefix.Value);
            }
            var faker = new Faker<Reviews>()
                          .CustomInstantiator(f => new Reviews(id++))
                           .RuleFor(o => o.description, f => f.PickRandom(itemsPrefix) + " " + f.PickRandom(cusions))
                           .RuleFor(o => o.yelp_id, f => f.PickRandom(yelpIds))
                          .RuleFor(o => o.user_id, f => f.PickRandom(userIds))
                          .RuleFor(o => o.img_key, "96DCABFD-BBFC-4941-8883-CEEDADEBDE4D.jpg")
                          .RuleFor(o => o.posted, f => f.Date.Recent(100))
                          .RuleFor(o => o.isindexed, true);

            var entities = faker.Generate(batchSize);

            var isProcessed = await BulkInsertAsync(entities);

            schedulerResponse = new SchedulerResponse
            {
                HttpStatusCode = isProcessed ? HttpStatusCode.OK : HttpStatusCode.InternalServerError,
                IsSuccess = isProcessed,
                Message = isProcessed ? $"Reviews mock data got inserted success from {sequenceNumber} to {sequenceNumber + batchSize} sequence number" : $"Reviews mock data got failed"
            };

            return schedulerResponse;
        }
        public async Task<SchedulerResponse> InsertVotesMockData(int batchSize)
        {
            SchedulerResponse schedulerResponse;

            var votes = _dbContext.Vote.ToList();
            var sequenceNumber = votes.Max(v => v.id);
            int id = sequenceNumber;
            var users = _dbContext.User.Select(r => r.id).ToList();
            var reviews = _dbContext.Review.Select(r => r.id).ToList();
            var votesList = new List<Votes>();
            foreach (var reviewId in reviews)
            {
                int votesPerReview = 0;
                foreach (var userId in users)
                {
                    var isVoted = votesList.Any(v => v.user_id.Equals(userId) && v.review_id.Equals(reviewId));
                    var isVotedAlready = votes.Any(v => v.user_id.Equals(userId) && v.review_id.Equals(reviewId));
                    if (batchSize > votesPerReview)
                    {
                        if (!isVoted && !isVotedAlready)
                        {
                            votesPerReview++;
                            Votes vote = new Votes
                            {
                                id = id++,
                                review_id = reviewId,
                                user_id = userId,
                                likes = (id % 2 == 0) ? true : false,
                                isindexed = true
                            };
                            votesList.Add(vote);
                        }
                    }
                }
            }

            var isProcessed = await BulkInsertAsync(votesList);

            schedulerResponse = new SchedulerResponse
            {
                HttpStatusCode = isProcessed ? HttpStatusCode.OK : HttpStatusCode.InternalServerError,
                IsSuccess = isProcessed,
                Message = isProcessed ? $"Votes mock data got inserted {batchSize} entries per review" : $"Votes mock data got failed"
            };

            return schedulerResponse;
        }
        public async Task<SchedulerResponse> InserFollowersMockData(int batchSize)
        {
            SchedulerResponse schedulerResponse;

            var users = _dbContext.User.Select(u => u.id).ToList();
            var followersIds = _dbContext.Follower.ToList();
            var sequenceNumber = followersIds.Max(f => f.id);
            int id = sequenceNumber;
            var followersList = new List<Followers>();
            foreach (var userId in users)
            {
                int followersCount = id;

                for (int i = 0; i < batchSize; i++)
                {
                    var random = new Random();
                    int followingUserId = random.Next(users.Count);
                    var isfollowedAlready = followersIds.Any(v => v.following_user_id.Equals(users[followingUserId]) && v.user_id.Equals(userId));
                    var isfollowed = followersList.Any(v => v.following_user_id.Equals(users[followingUserId]) && v.user_id.Equals(userId));

                    if (!isfollowed && !isfollowedAlready)
                    {

                        Followers follower = new Followers
                        {
                            id = followersCount++,
                            user_id = userId,
                            following_user_id = users[followingUserId],
                            isindexed = true
                        };
                        followersList.Add(follower);
                        id = follower.id;
                    }
                }
            }

            var isProcessed = await BulkInsertAsync(followersList);

            schedulerResponse = new SchedulerResponse
            {
                HttpStatusCode = isProcessed ? HttpStatusCode.OK : HttpStatusCode.InternalServerError,
                IsSuccess = isProcessed,
                Message = isProcessed ? $"Followers mock data got inserted {batchSize} entries per user" : $"Followers mock data got failed"
            };

            return schedulerResponse;
        }

        public async Task<SchedulerResponse> InsertCommentsMockData(int batchSize)
        {
            SchedulerResponse schedulerResponse;
            var users = _dbContext.User.Select(r => r.id).ToList();
            var reviews = _dbContext.Review.Select(r => r.id).ToList();
            var sequenceNumber = _dbContext.Comment.Max(u => u.id);
            int id = sequenceNumber;
            var commentsList = _configuration.GetSection("Review:Comments").GetChildren();

            var comments = new List<string>();
            foreach (var comment in commentsList)
            {
                if (!string.IsNullOrEmpty(comment.Value))
                    comments.Add(comment.Value);
            }

            var faker = new Faker<Comments>()
                            .CustomInstantiator(f => new Comments(id++))
                             .RuleFor(o => o.user_id, f => f.PickRandom(users))
                             .RuleFor(o => o.review_id, f => f.PickRandom(reviews))
                            .RuleFor(o => o.content, f => f.PickRandom(comments))
                            .RuleFor(o => o.created_at, f => f.Date.Recent(100))
                            .RuleFor(o => o.isindexed, true);

            var entity = faker.Generate(batchSize);

            var isProcessed = await BulkInsertAsync(entity);

            schedulerResponse = new SchedulerResponse
            {
                HttpStatusCode = isProcessed ? HttpStatusCode.OK : HttpStatusCode.InternalServerError,
                IsSuccess = isProcessed,
                Message = isProcessed ? $"Comments mock data got inserted success from {sequenceNumber} to {sequenceNumber + batchSize} sequence number" : $"Comments mock data got failed"
            };

            return schedulerResponse;
        }
        private async Task<bool> BulkInsertAsync<T>(List<T> entityList)
        {
            try
            {
                var bulkUploader = new NpgsqlBulkUploader(_dbContext);
                await bulkUploader.InsertAsync(entityList);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception in BulkInsertAsync:{ex}");
                return false;
            }
        }
    }
}
