﻿using Leysi.Schedulers.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public class FollowersRepository : IFollowersRepository
    {
        private readonly ILeysiDbContext _dbContext;
        public FollowersRepository(ILeysiDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<int> UpdateIndexing(Followers entity)
        {
            _dbContext.Follower.Update(entity);
            return await _dbContext.SaveChangesAsync();
        }
        public async Task<List<Followers>> GetIndexedRecords()
        {
            var result = await _dbContext.Follower.Where(u => u.isindexed == true).ToListAsync();
            return result;
        }      
    }
}
