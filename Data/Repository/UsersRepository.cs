﻿using Leysi.Schedulers.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public class UsersRepository : IUsersRepository
    {
        private readonly ILeysiDbContext _dbContext;
        public UsersRepository(ILeysiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> UpdateIndexing(Users entity)
        {
            _dbContext.User.Update(entity);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Users>> GetIndexedRecords()
        {
            var result = await _dbContext.User.Where(u => u.isindexed == true).ToListAsync();
            return result;
        }      
    }
}
