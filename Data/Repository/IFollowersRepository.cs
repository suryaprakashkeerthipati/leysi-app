﻿using Leysi.Schedulers.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public interface IFollowersRepository
    {
        Task<List<Followers>> GetIndexedRecords();
        Task<int> UpdateIndexing(Followers entity);
    }
}
