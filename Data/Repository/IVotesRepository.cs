﻿using Leysi.Schedulers.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public interface IVotesRepository
    {
        Task<List<Votes>> GetIndexedRecords();
        Task<int> UpdateIndexing(Votes entity);
    }
}
