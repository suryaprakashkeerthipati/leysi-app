﻿using Leysi.Schedulers.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data.Repository
{
    public class CommentsRepository : ICommentsRepository
    {
        private readonly ILeysiDbContext _dbContext;
        public CommentsRepository(ILeysiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> UpdateIndexing(Comments entity)
        {
            _dbContext.Comment.Update(entity);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Comments>> GetIndexedRecords()
        {
            var result = await _dbContext.Comment.Where(u => u.isindexed == true).ToListAsync();
            return result;
        }      
    }
}
