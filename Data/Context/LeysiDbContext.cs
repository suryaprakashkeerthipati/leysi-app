﻿using Leysi.Schedulers.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data
{
    public class LeysiDbContext : DbContext, ILeysiDbContext
    {
        public LeysiDbContext(DbContextOptions options)
             : base(options)
        {
        }

        public DbSet<Users> User { get; set; }
        public DbSet<Votes> Vote { get; set; }
        public DbSet<Reviews> Review { get; set; }
        public DbSet<Comments> Comment { get; set; }
        public DbSet<Followers> Follower { get; set; }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }

    }
}
