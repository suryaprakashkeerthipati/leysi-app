﻿using Leysi.Schedulers.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Leysi.Schedulers.Data.Context
{
    public class FakeDBContext: DbContext
    {
        public FakeDBContext(DbContextOptions<FakeDBContext> options) : base(options)
        { }

        public DbSet<Users> User { get; set; }
        public DbSet<Votes> Vote { get; set; }
        public DbSet<Reviews> Review { get; set; }
        public DbSet<Comments> Comment { get; set; }
        public DbSet<Followers> Follower { get; set; }
        public DbSet<Restaurants> Restaurant { get; set; }        
    }
}
