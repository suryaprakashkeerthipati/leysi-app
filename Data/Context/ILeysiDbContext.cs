﻿using Leysi.Schedulers.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Leysi.Schedulers.Data
{
    public interface ILeysiDbContext : IDisposable
    {
         DbSet<Users> User { get; set; }
         DbSet<Votes> Vote { get; set; }
         DbSet<Reviews> Review { get; set; }
         DbSet<Comments> Comment { get; set; }
         DbSet<Followers> Follower { get; set; }
        Task<int> SaveChangesAsync();
    }
}
